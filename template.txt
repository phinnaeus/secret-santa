Hello $giver!

Your Secret Santa match is $recipient.

Note that this email has been sent programatically and I have no
idea who your match is. Don't reply to this message because it will probably
include your match in the body text. Also, it will probably just bounce.
