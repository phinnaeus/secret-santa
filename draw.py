import sys
import argparse
import csv
import random
import smtplib
import getpass
import time
from string import Template
from email.mime.text import MIMEText


def read_names_file(input_file):
    """reads the given csv FD and returns a map of participant to email address"""
    participants = {}
    reader = csv.reader(input_file)
    for row in reader:
        if row[0] in participants:
            # todo names should not be required to be unique, email addresses should be though
            # currently names are required to be unique because they are used as keys to retrieve email addresses
            # a simple fix for this would be to assign an autoincrement id to each row in the input and use that instead
            print(f'Duplicate name [{row[0]}] found in input file')
            sys.exit(3)
        participants[row[0]] = row[1]
    return participants


def draw_matches(names):
    """a simple implementation of secret santa drawing that avoids dead ends and self-draws"""
    matches = {}
    random.shuffle(names)

    num_people = len(names)
    for i in range(num_people):
        matches[names[i]] = names[(i + 1) % num_people]

    return matches


def draw_matches_complex(names):
    """a more complex implementation that can generate more possible matches via brute forcing"""
    givers = names.copy()
    receivers = names.copy()

    attempts = 0
    while invalid(givers, receivers):
        random.shuffle(givers)
        random.shuffle(receivers)
        attempts += 1

    matches = {}
    for i in range(len(names)):
        matches[givers[i]] = receivers[i]

    print(f"it took {attempts} attempt(s) to generate valid matches")
    return matches


def invalid(givers, receivers):
    for i in range(len(givers)):
        if givers[i] == receivers[i]:
            return True

    return False


def generate_messages(matches, participants, from_email, subject, template, dryrun_address):
    """uses the given string Template to create a MIMEText message objects for each of the matches given"""
    messages = []
    for giver, recipient in matches.items():
        template_params = {
            'giver': giver,
            'recipient': recipient
        }
        msg = MIMEText(template.substitute(template_params))
        msg['Subject'] = subject
        msg['From'] = from_email
        msg['To'] = dryrun_address if dryrun_address else participants.get(giver)

        messages.append(msg)

    return messages


def send_emails(messages):
    """using Fastmail's SSL SMTP endpoint, sends the given messages

    this function prompts the user for a password which is then used to login

    todos:
    - get SMTP settings from arguments
    - support other email providers than fastmail
    - add a progress bar while sending messages, and possibly a brief sleep to avoid hammering SMTP servers
    """
    email_login = input('STMP Login: ')
    if not email_login:
        print('Login not provided. Aborting.')
        sys.exit(1)

    password = getpass.getpass('SMTP Password: ')
    if not password:
        print("Password not provided. Aborting.")
        sys.exit(2)

    with smtplib.SMTP_SSL('smtp.fastmail.com', 465) as server:
        server.login(email_login, password)

        for msg in messages:
            to_email = msg['To']
            server.sendmail(msg['From'], [to_email], msg.as_string())
            print(f'Sent message to {to_email}.')
            time.sleep(1)


def main():
    """main function

    1. parses command line arguments using argparse
    2. reads given input file for participants and email addresses
    3. generates matches using participants names
    4. generates MIMEText messages for each of the matches
    5. either sends emails or prints out the messages to the console
    """
    parser = argparse.ArgumentParser(
        description='Generate a set of Secret Santa drawing pairs')
    parser.add_argument('file', type=argparse.FileType('r'),
                        help='input file, CSV of name,email address for participants')
    parser.add_argument('--dryrun-to',
                        help='if set, sends all emails to this address instead of actual recipients')
    parser.add_argument('--send', action='store_true',
                        help='causes the program to send emails, use with caution')
    parser.add_argument('--better', action='store_true',
                        help='spend extra time to make a better random drawing')
    parser.add_argument('--print-matches', action='store_true',
                        help='only print out the matches generated and exit')
    email_group = parser.add_argument_group(
        'email', 'settings for sending emails')
    email_group.add_argument(
        '--from-email', help='the address to use as a sender')
    email_group.add_argument('--subject', default='[Automated] Secret Santa',
                             help='the subject of the emails to be sent out')
    email_group.add_argument('--template', type=argparse.FileType('r'), default='template.txt',
                             help='a python Template file to use as the body of the email sent out')
    email_group.add_argument('--backup-file', type=argparse.FileType('w'),
                             help='a filename to write a backup to, just in case')

    args = parser.parse_args()

    participants = read_names_file(args.file)
    args.file.close()

    names = list(participants.keys())
    if args.better:
        matches = draw_matches_complex(names)
    else:
        matches = draw_matches(names)

    if args.print_matches:
        for giver, recipient in matches.items():
            print(f'{giver} -> {recipient}')
        sys.exit(0)

    messages = generate_messages(
        matches,
        participants,
        args.from_email,
        args.subject,
        Template(args.template.read()),
        args.dryrun_to,
    )

    args.template.close()

    if args.backup_file:
        args.backup_file.write(str(matches))
        args.backup_file.close()

    if args.send:
        send_emails(messages)
    else:
        for msg in messages:
            print(msg.as_string())
            print('=' * 80)


if __name__ == "__main__":
    main()
